json.array!(@jewels) do |jewel|
  json.extract! jewel, :id, :picture, :description, :price, :available, :collection_id
  json.url jewel_url(jewel, format: :json)
end

class CreateJewels < ActiveRecord::Migration
  def change
    create_table :jewels do |t|
      t.string :picture
      t.text :description
      t.integer :price
      t.boolean :available
      t.references :collection, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
